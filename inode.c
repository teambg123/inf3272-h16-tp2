#include "inode.h"

int calculeBlocs(long);
INODE *newBlocs(long, INODE*);
INODE *noIndirection(int, long, char);
INODE *withIndirection(int, long, char);

INODE *newInode(long taille, char type) {
    int nbBloc = calculeBlocs(taille);
    if(nbBloc <= 10)
        return noIndirection(nbBloc, taille, type);
    else
        return withIndirection(nbBloc, taille, type);
}

void destroyInode(INODE *in){
    int i;
    for(i = 0; i < 10; i++)
        if(in->premiersBlocs[i] != -1)
            destroyBloc(in->premiersBlocs[i]);
    if(in->isimple != NULL)
        destroyISimple(in->isimple);
    else if(in->idouble != NULL)
        destroyIDouble(in->idouble);
    else
        destroyITriple(in->itriple);
}

int calculeBlocs(long taille) {
    return (taille+(_BLOC/2))/_BLOC; 
}

INODE *noIndirection(int nbBloc, long taille, char type) {
    INODE *ret = (INODE*)malloc(sizeof(INODE));
    if(ret == NULL)
        return NULL;
    //time_t rawtime;
    //ret->mode; koisse?
    ret->nbLiens = nbBloc;
    ret->UID = 0;
    ret->GID = 0;
    ret->taille = taille;
    ret->type = type;
    //ret->creation = localtime(&rawtime);
    //ret->acces = ret->creation;
    //ret->modification = ret->creation;
    ret = newBlocs(taille, ret); 

    return ret;
}

INODE *withIndirection(int nbBloc, long taille, char type){
    INODE *ret = noIndirection(nbBloc, taille, type);
    taille -= 10*_BLOC;
    if(taille < _POINTEUR*_BLOC){
        INDIRECTIONSIMPLE *is = newISimple(taille);
        if(is != NULL)
            ret->isimple = is;
        else
            ret = NULL;
    } else if (taille < _POINTEUR*_POINTEUR*_BLOC){
        INDIRECTIONDOUBLE *id = newIDouble(taille);
        if(id != NULL)
            ret->idouble = id;
        else
            ret = NULL;
    } else if (taille < (long)_POINTEUR*_POINTEUR*_POINTEUR*_BLOC){
        INDIRECTIONTRIPLE *it = newITriple(taille);
        if(it != NULL)
            ret->itriple = it;
        else
            ret = NULL;
    }
    return ret;
}

INODE *newBlocs(long taille, INODE *in){
    int i, index;
    for(i = 0; i < 10; i++)
        in->premiersBlocs[i] = -1;
    i = 0;
    while(i < 10 && taille > 0){
        if(taille > _BLOC){
            index =  newBloc(_BLOC);
            taille -= _BLOC; 
        } else {
            index = newBloc(taille);
            taille = 0;
        }
        if(index != -1)
            in->premiersBlocs[i] = index;
        else
            return NULL;
    }
    return in;
}

INDIRECTIONSIMPLE *newISimple(long taille){
    int i = 0, index;
    INDIRECTIONSIMPLE *is = (INDIRECTIONSIMPLE*)malloc(sizeof(INDIRECTIONSIMPLE));
    if(is == NULL)
        return NULL;
    for(i = 0; i < _POINTEUR; i++)
        is->no_bloc[i] = -1;
    i = 0;
    while(i < _POINTEUR && taille > 0){
        if(taille > _BLOC){
            index = newBloc(_BLOC);
            taille -= _BLOC;
        } else {
            index = newBloc(taille);
            taille = 0;
        }
        if(index != -1)
            is->no_bloc[i] = index;
        else 
            return NULL;
        i++;
    }
    return is;
}

void destroyISimple(INDIRECTIONSIMPLE *is){
    int i;
    for(i = 0; i < _POINTEUR; i++)
        if(i != -1)
            destroyBloc(is->no_bloc[i]);
    free(is);
}

INDIRECTIONDOUBLE *newIDouble(long taille){
    int i = 0;
    INDIRECTIONSIMPLE *is;
    INDIRECTIONDOUBLE *id = (INDIRECTIONDOUBLE*)malloc(sizeof(INDIRECTIONDOUBLE));
    if(id == NULL)
        return NULL;

    while(i < _POINTEUR && taille > 0) {
        if(taille > _POINTEUR*_BLOC){
            is = newISimple(_POINTEUR*_BLOC);
            taille -= _POINTEUR*_BLOC;
        } else {
            is = newISimple(taille);
            taille = 0;
        }
        if(is != NULL)
            id->is[i] = is;
        else
            return NULL;
        i++;
    }
    return id;
}

void destroyIDouble(INDIRECTIONDOUBLE *id){
    int i;
    for(i = 0; i < _POINTEUR; i++)
        destroyISimple(id->is[i]);
    free(id);
}

INDIRECTIONTRIPLE *newITriple(long taille){
    int i = 0;
    INDIRECTIONDOUBLE *id;
    INDIRECTIONTRIPLE *it = (INDIRECTIONTRIPLE*)malloc(sizeof(INDIRECTIONTRIPLE));
    if(it == NULL)
        return NULL;

    while(i < _POINTEUR && taille > 0) {
        if(taille > _POINTEUR*_POINTEUR*_BLOC){
            id = newIDouble(_POINTEUR*_POINTEUR*_BLOC);
            taille -= _POINTEUR*_POINTEUR*_BLOC;
        } else {
            id = newIDouble(taille);
            taille = 0;
        }
        if(id != NULL)
            it->id[i] = id;
        else
            return NULL;
        i++;
    }
    return it;
}

void destroyITriple(INDIRECTIONTRIPLE *it){
    int i;
    for(i = 0; i < _POINTEUR; i++)
        destroyIDouble(it->id[i]);
    free(it);
}
