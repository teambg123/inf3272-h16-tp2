#include "hdd.h"

int fetchFreeBloc();

static int *freeBlocs;

void initHDD(long memory){
    int i;
    MEMORY = memory;
    HDD = (BLOC*)malloc(sizeof(BLOC)*MEMORY);
    if(HDD == NULL)
        return;
    freeBlocs = (int*)malloc(sizeof(int)*MEMORY);
    if(freeBlocs == NULL)
        return;
    for(i = 0; i < MEMORY; i++)
        freeBlocs[i] = 1;
}

void destroyHDD(){
    free(HDD);
    free(freeBlocs);
}

int fetchFreeBloc(){
    int index = -1, i = 0;
    while(i < MEMORY && index == -1){
        if(freeBlocs[i] == 1)
            index = i;
        i++;
    }
    return index;
}

int newBloc(int taille){
    int index = fetchFreeBloc();
    if(index != -1){
        HDD[index].utilise = taille;
        freeBlocs[index] = 0;
        return index;
    } else
        return index;
}

void destroyBloc(int i){
    HDD[i].utilise = 0;
    freeBlocs[i] = 1;
}
