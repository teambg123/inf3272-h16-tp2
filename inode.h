#ifndef INODE_H
#define INODE_H

#define _POINTEUR 512

#include <stdlib.h>
#include <stdio.h>
#include<time.h>

#include "hdd.h"


typedef struct tm TEMPS;

typedef struct indirection_simple INDIRECTIONSIMPLE;
typedef struct indirection_double INDIRECTIONDOUBLE;
typedef struct indirection_triple INDIRECTIONTRIPLE;

typedef struct i_node INODE;

struct indirection_simple{
   int no_bloc[_BLOC];
};

struct indirection_double{
   INDIRECTIONSIMPLE* is[_BLOC];
};

struct indirection_triple{
   INDIRECTIONDOUBLE* id[_BLOC];
};

 
struct i_node{
  char mode[11];
  unsigned int nbLiens;
  int  UID;
  int  GID;
  unsigned int taille;
  TEMPS creation;
  TEMPS acces;
  TEMPS modification;
  char type;

  int premiersBlocs[10];
  INDIRECTIONSIMPLE* isimple;
  INDIRECTIONDOUBLE* idouble;
  INDIRECTIONTRIPLE* itriple;
};



INODE *newInode(long, char);
void destroyInode(INODE*);


INDIRECTIONSIMPLE *newISimple(long);
void destroyISimple(INDIRECTIONSIMPLE*);

INDIRECTIONDOUBLE *newIDouble(long);
void destroyIDouble(INDIRECTIONDOUBLE*);

INDIRECTIONTRIPLE *newITriple(long);
void destroyITriple(INDIRECTIONTRIPLE*); 

#endif
