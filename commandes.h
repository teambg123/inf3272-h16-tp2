#ifndef COMMANDES_H
#define COMMANDES_H

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>

#include "repGestion.h"

void commandeMkdir(char* chaine, REPERTOIRE *courant, REPERTOIRE *racine);
void commandeRmdir(char* chaine, REPERTOIRE *courant, REPERTOIRE *racine);
REPERTOIRE* commandeCd(char* chaine, REPERTOIRE *racine);
void commandeLs(char *, REPERTOIRE*, REPERTOIRE*, char*);
int commandeCrf(char* fichier, char* taille, REPERTOIRE *courant);
void commandeCp(char* fichier, char* chemin, REPERTOIRE* courant, REPERTOIRE* racine);
void commandeMv(char* fichier, char* chemin, REPERTOIRE* courant, REPERTOIRE* racine);
void commandeRm(char* fichier, REPERTOIRE* courant);
void commandeBlc(char* fichier, REPERTOIRE* courant);
void separerCheminRepRm(char* chaine, REPERTOIRE *courant, REPERTOIRE *racine);
void separerCheminRepMk(char* chaine, REPERTOIRE *racine);
char* extraireNomRep(char* chaine, char separateur);
int compteSlash(char* chaine);


#endif
