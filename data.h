#ifndef DATA_H
#define DATA_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "inode.h"
#include "repertoire.h"

typedef struct element_ligne_bloc{
  unsigned int position;
  char *nom;
  char type;
  INODE* inode;
  REPERTOIRE *rep; 
}LIGNEBLOC;

static int _INODE;
static LIGNEBLOC **iList;

void initINODEs(long);
void destroyINODEs();

int ajouterInodeFichier(long, char*, char);
int ajouterBlocRep(char*);
void supprimerInodeind(int);

LIGNEBLOC *getLB(int);

void renommerLB(LIGNEBLOC*, char*);
#endif
