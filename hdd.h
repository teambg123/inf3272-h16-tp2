#ifndef HDD_H
#define HDD_H

#define _BLOC 2048

#include <stdlib.h>
#include <stdio.h>

static int MEMORY;

/*
 * Capacite d'un bloc : 2048 octets
 * */
typedef struct element_fichier{
    unsigned short utilise;
}BLOC;

static BLOC *HDD;

void initHDD(long);
void destroyHDD();

int newBloc(int);
void destroyBloc(int);

#endif 
