#ifndef MAIN_H
#define MAIN_H


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <regex.h>

#include "commandes.h"
#include "doublePtrStruct.h"

static charTab_t *pwd;

void afficherPwd();

void nettoyerChaine(char *chaine);

int separerCmdOptions(char *entree, REPERTOIRE *courant, REPERTOIRE *racine);

int verifierParametres(char* commande, charTab_t *options);

int verifierString(char* chaine);

int verifierChemin(char* chaine);

void dispatchToCmd(char *commande, char* option1, char* option2, REPERTOIRE *courant, REPERTOIRE *racine);

void updatePath(char*);

#endif
