#include "data.h"

LIGNEBLOC *newLIGNEBLOC(int, char*, char, INODE*);
LIGNEBLOC *newLIGNEBLOCRep(int, char*, REPERTOIRE*);
void destroyLIGNEBLOC(LIGNEBLOC*);
int checkFreeInode();

void initINODEs(long taille){
    long inode, blocs;
    inode = (taille*100)/15;
    blocs = (taille*100)/85;
    
    initHDD(blocs);

    _INODE = inode;

    iList = (LIGNEBLOC**)malloc(sizeof(LIGNEBLOC)*_INODE);
    if(iList == NULL){
        printf("fail\n");
        return;
    }
}

void destroyINODEs(){
    destroyHDD();
    int i;
    for(i = 0; i < _INODE; i++)
        if(iList[i] != NULL)
            destroyLIGNEBLOC(iList[i]);
    free(iList);
}

int ajouterInodeFichier(long taille, char* nom, char type){
    int index = checkFreeInode(); 
    if(index != -1){
        INODE* in = newInode(taille, type);
        if(in != NULL){
            LIGNEBLOC *lb = newLIGNEBLOC(index, nom, 'f', in);
            if(lb != NULL)
                iList[index] = lb;
        }
    }
    return index;
}

int ajouterBlocRep(char *nom){
    int index = checkFreeInode();
    if(index != -1){
        REPERTOIRE *rep = newRepertoire();
        if(rep != NULL){
            LIGNEBLOC *lb = newLIGNEBLOCRep(index, nom, rep);
            if(lb != NULL)
                iList[index] = lb;
        }
    }
    return index;
}

int checkFreeInode(){
    int i;
    for(i = 0; i < _INODE; i++){
        if(iList[i] == NULL){
            return i;
        }
    }
    return -1;
}

void supprimerInodeind(int i){
    destroyLIGNEBLOC(iList[i]);
    iList[i] = NULL;
}


void renommerLB(LIGNEBLOC *lb, char *nom){
    free(lb->nom);
    lb->nom = strdup(nom);
}


LIGNEBLOC *newLIGNEBLOC(int index, char *nom, char type, INODE *in){
    LIGNEBLOC *lb = (LIGNEBLOC*)malloc(sizeof(LIGNEBLOC));
    if(lb == NULL)
        return NULL;

    lb->position = index;
    lb->nom = strdup(nom);
    lb->type = type;
    lb->inode = in;

    return lb;
}

LIGNEBLOC * newLIGNEBLOCRep(int index, char *nom, REPERTOIRE *rep){
    LIGNEBLOC *lb = newLIGNEBLOC(index, nom, 'd', NULL);
    lb->rep = rep;
    return lb;
}

void destroyLIGNEBLOC(LIGNEBLOC *lb){
    if(lb->inode != NULL)
        destroyInode(lb->inode);
    else
        destroyRepertoire(lb->rep);
    free(lb->nom);
    free(lb);
}

LIGNEBLOC *getLB(int i){
    return iList[i];
}
