#include "main.h"

//#define QUITTER quitter

const char* listeCommandes[] = {"mkdir" ,"rmdir" ,"cd" ,"ls" ,"crf" ,"cp" ,"mv" ,"rm" ,"blc", "logout"};


int main(int argc, char **argv){
    initINODEs(120000L);
    REPERTOIRE *racine = initRep();
    REPERTOIRE *courant = racine;
    char *entree = (char*)malloc(200*sizeof(char));
    if(entree == NULL)
        return -1;
    char quitter[] = "logout";
    pwd = newCharTab();
    pwd = addChar("/", pwd);
    while(strcmp(entree, quitter) !=0){
        afficherPwd();
        fgets(entree, 200, stdin);
        separerCmdOptions(entree, courant, racine);
        nettoyerChaine(entree);
    }
    free(entree);
    return 0;
}


void afficherPwd(){
    int i;
    for(i = 0;i < pwd->nbrElem; i++){
        printf("%s/", pwd->tableau[i]);
    }
    printf("# ");
}

void nettoyerChaine(char *chaine){
    int k = 0, taille = 0;
    taille = strlen(chaine);
    for(k=taille-1; k>0; k--)
    {
        if(!isalpha(chaine[k]))
            chaine[k]='\0';    
    }     
}

int separerCmdOptions(char *entree, REPERTOIRE *courant, REPERTOIRE *racine){
    int i = 0; 
    charTab_t *options = newCharTab();
    char *token;
    char *commande = (char*)malloc(201*sizeof(char));
    if(commande == NULL)
        return -1;
    token = strtok(entree, " ");
    commande = strdup(token);
    token = strtok(NULL, " ");
    while(token != NULL){
        options = addChar(token, options);
        token = strtok(NULL, " ");          
        if(options->nbrElem > 2){
            printf("Erreur : Trop d'options");
            return -1;
        }
        
    }
    nettoyerChaine(commande);
    if(options->nbrElem != 0)
        for(i=0; i<options->nbrElem; i++)
            nettoyerChaine(options->tableau[i]);
 
    int err = verifierParametres(commande, options);
    if(err >= 0)
        dispatchToCmd(commande, options->tableau[0], options->tableau[1], courant, racine);
    
    destroyCharTab(options);
    free(token);
    free(commande); 
    return err;
}

int verifierParametres(char* commande, charTab_t* options){
    int ret=0;
    int nombre = 0;
    if(options->nbrElem == 2){
        nombre = compteSlash(options->tableau[1]);
        if(nombre != 0)
            ret = ret + verifierChemin(options->tableau[1]);
        else
            ret = ret + verifierString(options->tableau[1]);
        nombre = compteSlash(options->tableau[0]);
        if(nombre != 0)
            printf("Erreur : Argument invalide\n");
        else
            ret = ret + verifierString(options->tableau[0]);
        
    }else if(options->nbrElem == 1){
        nombre = compteSlash(options->tableau[0]);
        if(nombre != 0){
            ret = ret + verifierChemin(options->tableau[0]);
        }else{
            ret = ret + verifierString(options->tableau[0]);
        }   
    }else if(options->nbrElem == 0 && strcmp(commande, "ls")!= 0 && strcmp(commande, "logout") != 0){
        ret += -1;
        printf("Erreur : Commande Invalide!\n");
    }
    return ret;
}

int verifierString(char* chaine){
    regex_t regex;
    if(regcomp(&regex, "([a-zA-Z0-9]|_|-|.)*", REG_EXTENDED|REG_NOSUB)){
        printf("L'initialisation de la regex a échoué\n");
        return -1;
    }
    if(regexec(&regex, chaine, (size_t) 0, NULL, 0) != 0){
        printf("Erreur : Nom de dossier/fichier invalide. Caratere autorises : [a-zA-Z0-9-_.]*"); 
        return -1;
    }
    return 0;
}

int verifierChemin(char* chaine){
    regex_t regex;
    int ret = -1;
    
    if(regcomp(&regex, "([/][a-zA-Z0-9-_.]*)*", REG_EXTENDED|REG_NOSUB)){
        printf("L'initialisation de la regex a échoué\n");
        return -1;
    }

    if(regexec(&regex, chaine, (size_t) 0, NULL, 0) != 0)
        ret = -1;
    else
        ret = 0;

    regfree(&regex);
    if(ret!=0)
        printf("Erreur : Chemin Invalide\n");
    return ret;
}



void dispatchToCmd(char *commande, char* option1, char* option2, REPERTOIRE *courant, REPERTOIRE *racine){
    if(strcmp(commande, listeCommandes[0]) == 0){
        if(option2 != NULL)
            printf("Erreur : Commande Invalide\n");
        else 
            commandeMkdir(option1, courant, racine);
    } else if(strcmp(commande, listeCommandes[1]) == 0){
        if(option2 != NULL)
            printf("Erreur : Commande Invalide\n");
        else 
            commandeRmdir(option1, courant, racine);
    } else if(strcmp(commande, listeCommandes[2]) == 0){
        if(option2 != NULL)
            printf("Erreur : Commande Invalide\n");
        else{ 
            commandeCd(option1, racine);
            updatePath(option1);
        }
    } else if(strcmp(commande, listeCommandes[3]) == 0){
        if(option2 != NULL && option1 != NULL && strcmp(option1, "-l") != 0)
            printf("Erreur : Commande Invalide\n");
        else
            commandeLs(option2, courant, racine, option1);
    } else if(strcmp(commande, listeCommandes[4]) == 0){
        if(option2 == NULL || option1 == NULL)
            printf("Erreur : Commande Invalide\n");
        else
            commandeCrf(option1, option2, courant);
    } else if(strcmp(commande, listeCommandes[5]) == 0){    
        if(option2 == NULL || option1 == NULL)
            printf("Erreur : Commande Invalide\n");
        else
            commandeCp(option1, option2, courant, racine);
    } else if(strcmp(commande, listeCommandes[6]) == 0){ 
        if(option2 == NULL || option1 == NULL)
            printf("Erreur : Commande Invalide\n");
        else 
            commandeMv(option1, option2, courant, racine);
    } else if(strcmp(commande, listeCommandes[7]) == 0){
        if(option2 != NULL)
            printf("Erreur : Commande Invalide\n");
        else 
            commandeRm(option1, courant);
    } else if(strcmp(commande, listeCommandes[8]) == 0){ 
        if(option2 != NULL)
            printf("Erreur : Commande Invalide\n");
        else 
            commandeBlc(option1, courant);
    } else if(strcmp(commande, listeCommandes[9]) == 0)
        printf("Fin du programme\n");
    else
        printf("Erreur : Commande Invalide!\n");
}

void updatePath(char *option){
    int i;
    if(strcmp(option, "..") == 0)
        removeLast(pwd);
    else if(strcmp(option, "/") == 0){
        for(i = 0; i < pwd->nbrElem; i++)
            removeLast(pwd);
        addChar("/", pwd);
    } else {
        for(i = 0; i < pwd->nbrElem; i++)
            removeLast(pwd);
        char s[2] = "/";
        char *token = strtok(option, s);
        while(token != NULL){
            addChar(token, pwd);
            token = strtok(NULL, s);
        }
    }

}
