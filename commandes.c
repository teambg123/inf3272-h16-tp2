#include "commandes.h"


void afficherLs(int);
void lssimple(REPERTOIRE*);
void lsdetail(REPERTOIRE*);
void blcIS(INDIRECTIONSIMPLE*);
void blcID(INDIRECTIONDOUBLE*);
void blcIT(INDIRECTIONTRIPLE*);

void commandeMkdir(char *chaine, REPERTOIRE* courant, REPERTOIRE* racine){
    if(compteSlash(chaine) !=0)
        separerCheminRepMk(chaine, racine);
    else
        creerRepertoire(chaine, courant);
}


void commandeRmdir(char* chaine, REPERTOIRE* courant, REPERTOIRE* racine){
    int indexLB = 0;
    if(compteSlash(chaine) !=0)
        separerCheminRepRm(chaine, courant, racine);
    else{
        LIGNEBLOC *LB = fetchLB(courant, chaine);
        if(LB->type == 'd'){
            if(isEmpty(LB->rep) == 0){
                indexLB = detacherLB(courant, LB->position);
                supprimerInodeind(indexLB);
            }
        } else
            printf("Erreur : L'entree n'est pas un repertoire\n");
    }
}


REPERTOIRE* commandeCd(char* chaine, REPERTOIRE *racine){
    if(strcmp(chaine, "..") == 0)
        return parseCheminParent(chaine, racine);
    else if(strcmp(chaine, "/") == 0)
        return racine;
    else
        return parseChemin(chaine, racine);
}


void commandeLs(char *chemin, REPERTOIRE *courant, REPERTOIRE *racine, char *option){
    if(chemin != NULL)
        courant = parseChemin(chemin, racine);
    if(strcmp(option, "-l") == 0)
        lsdetail(courant);
    else
        lssimple(courant);
}

void lsdetail(REPERTOIRE *courant){
    int i;
    LISTEFICHIER *lf;
    if(isEmpty(courant) != 0){
        lf = courant->content;
        do{
            for(i = 0; i < 6; i++)
                if(courant->content->liste[i] != -1)
                    afficherLs(lf->liste[i]);
            if(lf->suivant != NULL)
                lf = lf->suivant;
        }while(lf->suivant != NULL);
    }
}

void lssimple(REPERTOIRE *courant){
    int i;
    LISTEFICHIER *lf;
    if(isEmpty(courant) != 0){
        lf = courant->content;
        do{
            for(i = 0; i < 6; i++)
                if(lf->liste[i] != -1)
                    printf("%s\n", getLB(lf->liste[i])->nom);
            if(lf->suivant != NULL)
                lf = lf->suivant;
        }while(lf->suivant != NULL);
    }
}

void afficherLs(int indexLB){
    LIGNEBLOC *lb = getLB(indexLB);
    if(lb->type == 'f')
        printf("%c%s %d %d %d %s\n", lb->type, lb->inode->mode, lb->inode->taille, lb->inode->UID, lb->inode->GID, lb->nom);
    else
        printf("%c--------- - 1 1 %s\n", lb->type, lb->nom);
}

int commandeCrf(char* fichier, char* taille, REPERTOIRE* courant){
    int indice =0;
    long tailleInt =0;
    char type;
    char* typeStr;
    
    typeStr = strrchr(fichier, '.');
    
    if(isalpha(typeStr[1]) && strlen(typeStr) == 2){
        type = typeStr[1];
    }else{
        printf("Erreur : Type incorrect!");
        return -1;
    }
    
    tailleInt = strtoumax(taille, NULL, 10);
    if(tailleInt>=0){
        indice = ajouterInodeFichier(tailleInt, fichier, type);
        return attacherLB(courant, indice);
    }else{
        return -1;
    }   
}


void commandeCp(char* fichier, char* chemin, REPERTOIRE* courant, REPERTOIRE* racine){
    int index = 0, diffTailles = 0, j=0;
    char* nom = NULL, *cpy = NULL;
    char typeSrc;
    long inodeSrc;
    LIGNEBLOC* fichierADupp;
    REPERTOIRE* destRep;

    fichierADupp = fetchLB(courant, fichier);
    if(fichierADupp->type == 'f'){
        inodeSrc = fichierADupp->inode->taille;
        typeSrc=fichierADupp->inode->type;
        if(compteSlash(chemin) !=0){
            nom = extraireNomRep(chemin, '/');
        
            diffTailles = strlen(chemin) - strlen(nom);    
            cpy = (char*)malloc(sizeof(char)*diffTailles);
            if(cpy == NULL)
                return;
            for(j=0; j<diffTailles+1; j++){
                cpy[j]=chemin[j];
            }
            destRep = parseChemin(cpy, racine);
            index = ajouterInodeFichier(inodeSrc, nom, typeSrc);

            if(attacherLB(courant, index)!= 0)
                printf("Erreur : ");
        }else{
            index = ajouterInodeFichier(inodeSrc, nom, typeSrc);
        
            if(attacherLB(courant, index)!= 0)
                printf("Erreur : ");
        }
    } else
        printf("Erreur : L'entree n'est pas un fichier\n");
}


void commandeMv(char* fichier, char* chemin, REPERTOIRE* courant, REPERTOIRE* racine){
    
    int diffTailles = 0, j=0;
    int posSrc = 0, inodeSrc = 0;
    char* nom = NULL, *cpy = NULL;
    LIGNEBLOC* fichierADepl;
    REPERTOIRE* destRep = NULL;
  
    fichierADepl = fetchLB(courant, fichier);
    if(fichierADepl->type == 'f'){
        inodeSrc = fichierADepl->inode->taille;
        posSrc = fichierADepl->position;
        if(compteSlash(chemin) !=0){
            nom = extraireNomRep(chemin, '/');
            diffTailles = strlen(chemin) - strlen(nom);
            cpy = (char*)malloc(sizeof(char)*diffTailles);
            if(cpy == NULL)
                return;    
            for(j=0; j<diffTailles+1; j++){
                cpy[j]=chemin[j];
            }
            destRep = parseChemin(cpy, racine);
            deplacerLB(destRep, courant, posSrc);
        }else{
            renommerLB(fichierADepl, nom);
        }
    } else
        printf("Erreur : L'entree n'est pas un fichier\n");
}


void commandeRm(char* fichier, REPERTOIRE* courant){
    LIGNEBLOC* fichierASupp;
    int index;
    fichierASupp = fetchLB(courant, fichier);
    if(fichierASupp->type == 'f'){
        if(fichierASupp != NULL){
            index = detacherLB(courant,fichierASupp->position);
            supprimerInodeind(index);
        }else{
            printf("Erreur : fichier introuvable!\n");
        }
    }else
        printf("Erreur : l'entree n'est pas un fichier\n");
}


void commandeBlc(char* fichier, REPERTOIRE* courant){
    
    int i=0;
    LIGNEBLOC* fichierALire;
    INODE* inodeALire;

    fichierALire = fetchLB(courant, fichier);
    if(fichierALire->type == 'f'){    
        inodeALire = fichierALire->inode;
        printf("Numéros de blocs alloués\n");
        for(i=0; i<10; i++){ 
            if(inodeALire->premiersBlocs[i] != -1)
                printf("Bloc : %d\n", inodeALire->premiersBlocs[i]);
        }
        if(inodeALire->isimple != NULL){
            blcIS(inodeALire->isimple);
        }else if(inodeALire->idouble != NULL){
            blcID(inodeALire->idouble);
        }else if(inodeALire->itriple != NULL){
            blcIT(inodeALire->itriple);
        }
    } else
        printf("Erreur : l'entree n'est pas un fichier\n");
}

void blcIS(INDIRECTIONSIMPLE *is){
    int i;
    for(i = 0; i < _POINTEUR; i++)
        if(is->no_bloc[i] != -1)
            printf("Bloc : %d\n", is->no_bloc[i]);
}

void blcID(INDIRECTIONDOUBLE *id){
    int i;
    for(i = 0; i < _POINTEUR; i++)
        blcIS(id->is[i]);
}

void blcIT(INDIRECTIONTRIPLE *it){
    int i;
    for(i = 0; i < _POINTEUR; i++)
        blcID(it->id[i]);
}























void separerCheminRepRm(char *chaine, REPERTOIRE *courant, REPERTOIRE* racine)
{
    REPERTOIRE *repSupp = NULL;

    char *nomRepSupp = extraireNomRep(chaine, '/');
    repSupp = parseChemin(nomRepSupp, racine);
    LIGNEBLOC *lb = fetchLB(courant, nomRepSupp);
    if(isEmpty(lb->rep) == 0 && lb->type == 'd'){
        int indexLB = detacherLB(courant, lb->position);
        supprimerInodeind(indexLB);
    }
    free(nomRepSupp);
}


void separerCheminRepMk(char *chaine, REPERTOIRE* racine)
{
    REPERTOIRE *parent = NULL;
    
    int diffTailles = 0; 
    int j=0;
    char *nouvRep;
    char *chemin = (char*)malloc(201*sizeof(char));
    if(chemin == NULL)
        return;

    nouvRep = extraireNomRep(chaine, '/');
    
    diffTailles = strlen(chaine) - strlen(nouvRep);    
    for(j=0; j<diffTailles+1; j++)
    {
        chemin[j]=chaine[j];
    }
    parent = parseChemin(chemin, racine);
    creerRepertoire(nouvRep, parent);
    free(nouvRep);
    free(chemin);
}

char* extraireNomRep(char* chaine, char separateur){
    int i=0;
    char *repertoire = strrchr(chaine, separateur);
    char *ret = (char*)malloc(sizeof(char)*(strlen(repertoire) - 1));
    if(ret == NULL)
        return NULL;
    for(i = 0; i < (int)strlen(repertoire); i++)
    {
        if(repertoire[i] != separateur)
            ret[i] = repertoire[i];
    }
    return repertoire;
}



int compteSlash(char *chaine)
{ 
    int compteur = 0, i = 0;
    for(i=0; i<(int)strlen(chaine); i++)
    {
        if(chaine[i] == '/')
            compteur++;
    }
    return compteur;
}
