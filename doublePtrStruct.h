/*
 * Ce module provient de notre TP precedent.
 * */

#ifndef DOUBLE_PTR_H
#define DOUBLE_PTR_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "menu.h"
#include "repertoire.h"

#define _CHUNK 10

/*
 * Tableau de char avec sa taille
 * */
typedef struct charTab charTab_t;

struct charTab {
    char **tableau;
    int factor;
    int nbrElem;
};

charTab_t *newCharTab();
void destroyCharTab(charTab_t*);
charTab_t *addChar(char*, charTab_t*);
charTab_t *removeLast(charTab_t*);
int ctIsEmpty(charTab_t*);
charTab_t *reallocTab(charTab_t*);

#endif
