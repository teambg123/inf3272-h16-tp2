#include "repGestion.h"

char *nettoyerchar(char*);
REPERTOIRE *chercherRep(charTab_t*, REPERTOIRE*);
REPERTOIRE *chercherListeRep(REPERTOIRE*, char*);

LIGNEBLOC *fetchLBinLF(LISTEFICHIER*, char*);
int attacherINODE(LISTEFICHIER*, int);
int detacherINODE(LISTEFICHIER*, int);


int creerRepertoire(char *nom, REPERTOIRE *parent){
    int indexLB = ajouterBlocRep(nom);
    LIGNEBLOC *lb = getLB(indexLB);
    return attacherLB(parent, indexLB);
}

REPERTOIRE *initRep(){
    int i = ajouterBlocRep("/");
    if(i != -1){
        LIGNEBLOC *lb = getLB(i);
        return lb->rep;
    }
    return NULL;
}


REPERTOIRE *parseCheminParent(char *chemin, REPERTOIRE *racine){
    int i;
    char *enfant = strrchr(chemin, '/');
    int _parent = (int)strlen(chemin) - (int)strlen(enfant);
    char *parent = (char*)malloc(_parent);
    if(parent == NULL)
        return NULL;
    for(i = 0; i < _parent; i++)
        parent[i] = chemin[i];
    REPERTOIRE *ret = parseChemin(parent, racine);
    free(parent);
    return ret;
}

REPERTOIRE *parseChemin(char* chaine, REPERTOIRE *racine){
    char *token, s[2]="/";
    charTab_t *reps = newCharTab();
    if(reps == NULL)
        return NULL;
    token = strtok(chaine, s);
    while(token != NULL){
        token = nettoyerchar(token);
        addChar(token, reps);
        token = strtok(NULL, s);
    }
    return chercherRep(reps, racine);
}

REPERTOIRE *chercherRep(charTab_t *chemin, REPERTOIRE *racine){
    REPERTOIRE *courant = racine;
    LIGNEBLOC *lb;
    int i;
    for(i = 0; i < chemin->nbrElem; i++){
        lb = fetchLB(courant, chemin->tableau[i]);
        courant = lb->rep;
        if(lb->type != 'd'){
            printf("Erreur, L'entree est un fichiert\n");
            return NULL;
        }
    }
    return courant;
}


char *nettoyerchar(char *chaine){
    int i;
    for(i = (int)strlen(chaine)-1; i > 0; i--){
        if(chaine[i] == '/' || chaine[i] == '\n')
            chaine[i] = '\0';
    }
    return chaine;
}

int isEmpty(REPERTOIRE *rep){
    LISTEFICHIER *lf = rep->content;
    if(lf->liste[0] != -1)
        return 0;
    else
        return 1;
}

int attacherLB(REPERTOIRE *rep, int indexLB){
    return attacherINODE(rep->content, indexLB);
}

int attacherINODE(LISTEFICHIER *lf, int indexLB){
    int index = checkFreeLP(lf);
    if(index != -1)
        lf->liste[index] = indexLB;
    else if(lf->suivant == NULL){
        LISTEFICHIER *newLP = newLISTEREP();
        if(newLP == NULL)
            return -1;
        newLP->liste[0] = indexLB;
        lf->suivant = newLP;
    } else
        attacherINODE(lf->suivant, indexLB);
    return 0;
}


int detacherLB(REPERTOIRE *rep, int indexLB){
    return detacherINODE(rep->content, indexLB);
}

int detacherINODE(LISTEFICHIER *lf, int indexLB){
    int index = fetchIndexLB(lf, indexLB);
    if(index != -1){
        lf->liste[index] = -1;
        return indexLB;
    } else
        return detacherINODE(lf->suivant, indexLB);
}


int deplacerLB(REPERTOIRE *source, REPERTOIRE *dest, int indexLB){
    detacherLB(source, indexLB);
    return attacherLB(dest, indexLB);
}

LIGNEBLOC *fetchLB(REPERTOIRE *rep, char *nom){
    return fetchLBinLF(rep->content, nom);
}

LIGNEBLOC *fetchLBinLF(LISTEFICHIER *lf, char *nom){
    int i;
    for(i = 0; i < 6; i++){
        char *LBnom = getLB(lf->liste[i])->nom;
        if(strcmp(LBnom, nom) == 0)
            return getLB(lf->liste[i]);
    }
    if(lf->suivant != NULL)
        return fetchLBinLF(lf->suivant, nom);
    else
        return NULL;
}
