#include "doublePtrStruct.h"

charTab_t *newCharTab() {
    charTab_t *retour = (charTab_t*)malloc(sizeof(charTab_t));
    if(retour == NULL)
        return NULL;
    retour->tableau = (char**)malloc(sizeof(char)*_CHUNK);
    if(retour->tableau == NULL) {
        free(retour);
        return NULL;
    }
    retour->nbrElem = 0;
    return retour;
}

void destroyCharTab(charTab_t *donnees) {
    free(donnees->tableau);
    free(donnees);
}

charTab_t *addChar(char *chaine, charTab_t *tab){
    if(tab->nbrElem >= tab->factor*_CHUNK)
        tab = reallocTab(tab);
    if(tab != NULL){
        tab->tableau[tab->nbrElem++] = strdup(chaine);
    }
    return tab;
}

charTab_t *removeLast(charTab_t *tab){
    if(tab->nbrElem > 0){
        tab->tableau[(tab->factor*tab->nbrElem)-1] = NULL;
        tab->nbrElem -= 1;
    }
    return tab;
}

charTab_t *reallocTab(charTab_t *tab){
    tab->factor++;
    char **temp = (char**)realloc(tab->tableau, tab->factor*_CHUNK);
    if(temp == NULL)
        return NULL;
    tab->tableau = temp;
    return tab;
}
