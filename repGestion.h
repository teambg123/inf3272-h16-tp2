#ifndef REPGESTION_H
#define REPGESTION_H

#include <stdlib.h>
#include <stdio.h>
#include "data.h"

/*
 * Cree le repertoire Racine
 * */
REPERTOIRE *initRep();

/*
 * Creer un repertoire 
 * char *nom
 * retourne la position
 * */
int creerRepertoire(char*, REPERTOIRE*);

/*
 * Parse un chemin (valide) et retourne le repertoire correspondant.
 * */
REPERTOIRE *parseChemin(char*, REPERTOIRE*);
/*
 * Parse un chemin (valide) et retourne le parent du repertoire correspondant.
 * */
REPERTOIRE *parseCheminParent(char*, REPERTOIRE*);

int isEmpty(REPERTOIRE*);

/*
 * Ajouter un fichier a un repertoire
 * REPERTOIRE *parent, int indexLB(enfant)
 * Return -1 si erreur, sinon 0.
 * */
int attacherLB(REPERTOIRE*, int);
int detacherLB(REPERTOIRE*, int);
int deplacerLB(REPERTOIRE*, REPERTOIRE*, int);

/*
 * Retourne le LISTEBLOC contenu dans un dossier.
 * */
LIGNEBLOC *fetchLB(REPERTOIRE*, char*);

#endif
