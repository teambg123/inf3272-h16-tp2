#include "repertoire.h"
    

REPERTOIRE *newRepertoire(){
    REPERTOIRE *rep = (REPERTOIRE*)malloc(sizeof(REPERTOIRE));
    if(rep == NULL)
        return NULL;
    rep->bloc = newBloc(_BLOC);
    rep->content = newLISTEREP();
    if(rep->content == NULL)
        return NULL;
    return rep;
}

void destroyRepertoire(REPERTOIRE *rep){
    destroyLISTEREP(rep->content);
    destroyBloc(rep->bloc);
    free(rep);
}

LISTEFICHIER *newLISTEREP(){
    int i;
    LISTEFICHIER *lf = (LISTEFICHIER*)malloc(sizeof(LISTEFICHIER));
    if(lf == NULL)
        return NULL;
    for(i = 0; i < 6; i++)
        lf->liste[i] = -1;
    return lf;
}

void destroyLISTEREP(LISTEFICHIER *lf){
    if(lf->suivant == NULL)
        free(lf);
    else
        destroyLISTEREP(lf->suivant);
}

int checkFreeLP(LISTEFICHIER *lf){
    int i;
    for(i = 0; i < 6; i++)
        if(lf->liste[i] == -1)
            return i;
    return -1;
}

int fetchIndexLB(LISTEFICHIER *lf, int indexLB){
    int i;
    for(i = 0; i < 6; i++)
        if(lf->liste[i] == indexLB)
            return i;
    return -1;
}
