#ifndef REPERTOIRE_H
#define REPERTOIRE_H

#include <stdlib.h>
#include <stdio.h>
#include "doublePtrStruct.h"
#include "hdd.h"

typedef struct element_repertoire REPERTOIRE;

typedef struct element_liste_bloc{
  int liste[6];
  struct element_liste_bloc* suivant;
} LISTEFICHIER;

struct element_repertoire{
  int bloc;
  LISTEFICHIER* content;
};

static REPERTOIRE *repList;

/*
 * Cree un nouveau repertoire avec le nom et le parent
 * */
REPERTOIRE *newRepertoire();
void destroyRepertoire(REPERTOIRE*);
LISTEFICHIER *newLISTEREP();
void destroyLISTEREP(LISTEFICHIER *lp);

int checkFreeLP(LISTEFICHIER*);
int fetchIndexLB(LISTEFICHIER*, int);
#endif
